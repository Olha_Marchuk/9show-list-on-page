/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
 Методом createElement()
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і 
опишіть можливі варіанти цього параметра.
Перший параметр вказанує куди саме потрібно вставити елемент.
- beforebegin - ставити безпосередньо перед елементом
- afterbegin - на початок елемента
- beforeend - в кінець елементу
- afterend - після елементу
3. Як можна видалити елемент зі сторінки?
Для видалення існує метод remove - elem.remove()
 */
const getArrayList = (arr, domElem = document.body) =>{
    const ul = document.createElement('ul')
    if(domElem !== document.body){
        const domItem = document.createElement(domElem)
        console.log(domItem)
        document.body.append(domItem)
        domItem.append(ul)
    }else{
        document.body.append(ul)
    }
    
    const htmlArr = arr.map(element => `<li>${element}</li>`);
    return ul.insertAdjacentHTML('beforeend', htmlArr.join(''))
}
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


getArrayList(arr, 'div')